package epamjava.classes.task2.comparators;

import epamjava.classes.task2.stones.Stone;

import java.util.Comparator;

public class StoneComparator implements Comparator<Stone> {

    @Override
    public int compare(Stone o1, Stone o2) {
        return Double.compare(o1.getPrice(), o2.getPrice());
    }
}
