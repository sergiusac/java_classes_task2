package epamjava.classes.task2;

import epamjava.classes.task2.stones.PreciousStone;
import epamjava.classes.task2.stones.SemiPreciousStone;
import epamjava.classes.task2.stones.Stone;

import java.util.Arrays;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        List<Stone> stones = Arrays.asList(
            new PreciousStone("Ruby", 0.8, 250, 350),
            new PreciousStone("Ruby", 0.83, 245, 350),
            new PreciousStone("Sapphire", 0.78, 150, 450),
            new PreciousStone("Sapphire", 0.71, 115, 380),
            new SemiPreciousStone("Aventurine", 0.14, 221, 80),
            new SemiPreciousStone("Aquamarine", 0.2, 273, 95),
            new SemiPreciousStone("Jade", 0.18, 305, 78)
        );
        Jewellery jewellery = new Jewellery(stones);

        System.out.println("Created jewellery:");
        System.out.println(jewellery);

        System.out.println(String.format("Jewellery Cost: %f", jewellery.calculatePrice()));
        System.out.println(String.format("Jewellery Weight: %f", jewellery.calculateWeight()));

        System.out.println("Stones sorted by price:");
        System.out.println(jewellery.sortStonesByPrice());

        System.out.println("Stones with transparency from 0.7 to 0.8:");
        System.out.println(jewellery.filterStonesByTransparency(0.7, 0.8));
    }
}
